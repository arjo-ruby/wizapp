'use strict';
app.factory('gradebookService', ['$http', function ($http) {
 
    var serviceBase = 'http://104.236.116.175:8090/oauth/token';
    var gradebookServiceFactory = {};
 
    var _getOrders = function () {
 
        return $http.get(serviceBase + 'user/student').then(function (results) {
            return results;
        });
    };
 
    gradebookServiceFactory.getGrades = _getGrades;
 
    return gradebookServiceFactory;
 
}]);