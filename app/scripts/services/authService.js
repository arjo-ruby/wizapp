'use strict';
app.factory('authService', ['Base64','$http', '$q', 'localStorageService', function (Base64, $http, $q, localStorageService) {
 
    var serviceBase = 'http://104.236.116.175:8090/oauth/';
    var authServiceFactory = {};
 
    var _authentication = {
        isAuth: false,
        userName : ""
    };

 
    var _login = function (loginData) {

        var theauthdata = Base64.encode('clientapp'+':'+'123456');
 
        // For URL encoded 
        //var data = "password=" + loginData.password + "&username=" + loginData.userName + "&grant_type=password&scope=read%20write&client_secret=123456&client_id=clientapp" ;
        
      //  var data = { username : loginData.userName , password: loginData.password , client_id : 'clientapp', client_secret: '123456', grant_type: 'password', scope: 'read write'};
        //JSON 
        // var data = {
        //     'username' : loginData.userName,
        //     'password' : loginData.password,
        //     'grant_type' : 'password',
        //     'scope' : 'read write' ,
        //     'client_id': 'clientapp',
        //     'client_secret' : '123456'
        // }


        var deferred = $q.defer();
        
        var headers2 = {
            'Accept' : 'application/json',
            'Authorization': 'Basic ' + theauthdata,
            'Content-Type': 'application/x-www-form-urlencoded'
        };
   
        // $http.post(serviceBase + 'token', data, { headers: headers2}, {beforeSend: function(xhr){xhr.setRequestHeader('Authorization', 'Basic '+ theauthdata); }})

        var xsrf = $.param({
                username : loginData.userName,
                password : loginData.password,
                scope : "read write",
                grant_type : 'password',
                client_id : 'clientapp',
                client_secret : '123456'
             });

        console.log(xsrf);

        $http({
             method : 'POST',
             headers: headers2,
             url : serviceBase + 'token', 
             transformRequest: function(obj) {
                var str = [];
                for(var p in obj)
                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                return str.join("&");
            },

             data : xsrf 

            // withCredentials:true
            
            // {beforeSend: function(xhr)({ xhr.setRequestHeader('Authorization', 'Basic '+ theauthdata); })}
            })

        .then(function (response) 
        {
            console.log(response.data);
 
            localStorageService.set('authorizationData', { token: response.access_token, userName: loginData.userName });
 
            _authentication.isAuth = true;
            _authentication.userName = loginData.userName;
 
            deferred.resolve(response);
 
        },function (err, status) {
            // console.log(response.data);
            _logOut();
            deferred.reject(err);
        });
 
        return deferred.promise; 

            // Jquery implementation try

            // var data="password=check&username=admin&grant_type=password&scope=read%20write&client_secret=123456&client_id=clientapp"
            // var url = 'http://104.236.116.175:8090/oauth/token';
            // var username="clientapp";
            // var password="123456";
            // var authHeader=make_base_auth(username, password);

            // $.ajax( {url:url,
            //          type:"POST",
            //          data:data,
            //          headers: {'Accept' : 'application/json', "Authorization" : authHeader},
            //          beforeSend: function (xhr){xhr.setRequestHeader('Authorization', make_base_auth(username, password)); },
            //          success:function(data, textStatus, jqXHR) {alert("success");},
            //          error: function(jqXHR, textStatus, errorThrown) {alert("failure");}
            // });

            // function make_base_auth(user, password) {
            //   var tok = user + ':' + password;
            //   var hash = btoa(tok);
            //   return "Basic " + hash;
            // }

    };
 
    var _logOut = function () {
 
        localStorageService.remove('authorizationData');
 
        _authentication.isAuth = false;
        _authentication.userName = "";
 
    };
 
    var _fillAuthData = function () {
 
        var authData = localStorageService.get('authorizationData');
        if (authData)
        {
            _authentication.isAuth = true;
            _authentication.userName = authData.userName;
        }
 
    }

 
    // authServiceFactory.saveRegistration = _saveRegistration;
    authServiceFactory.login = _login;
    authServiceFactory.logOut = _logOut;
    authServiceFactory.fillAuthData = _fillAuthData;
    authServiceFactory.authentication = _authentication;
 
    return authServiceFactory;
}])


.factory('Base64', function () {
    /* jshint ignore:start */
  
    var keyStr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
  
    return {
        encode: function (input) {
            var output = "";
            var chr1, chr2, chr3 = "";
            var enc1, enc2, enc3, enc4 = "";
            var i = 0;
  
            do {
                chr1 = input.charCodeAt(i++);
                chr2 = input.charCodeAt(i++);
                chr3 = input.charCodeAt(i++);
  
                enc1 = chr1 >> 2;
                enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                enc4 = chr3 & 63;
  
                if (isNaN(chr2)) {
                    enc3 = enc4 = 64;
                } else if (isNaN(chr3)) {
                    enc4 = 64;
                }
  
                output = output +
                    keyStr.charAt(enc1) +
                    keyStr.charAt(enc2) +
                    keyStr.charAt(enc3) +
                    keyStr.charAt(enc4);
                chr1 = chr2 = chr3 = "";
                enc1 = enc2 = enc3 = enc4 = "";
            } while (i < input.length);
  
            return output;
        },
  
        decode: function (input) {
            var output = "";
            var chr1, chr2, chr3 = "";
            var enc1, enc2, enc3, enc4 = "";
            var i = 0;
  
            // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
            var base64test = /[^A-Za-z0-9\+\/\=]/g;
            if (base64test.exec(input)) {
                window.alert("There were invalid base64 characters in the input text.\n" +
                    "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
                    "Expect errors in decoding.");
            }
            input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
  
            do {
                enc1 = keyStr.indexOf(input.charAt(i++));
                enc2 = keyStr.indexOf(input.charAt(i++));
                enc3 = keyStr.indexOf(input.charAt(i++));
                enc4 = keyStr.indexOf(input.charAt(i++));
  
                chr1 = (enc1 << 2) | (enc2 >> 4);
                chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                chr3 = ((enc3 & 3) << 6) | enc4;
  
                output = output + String.fromCharCode(chr1);
  
                if (enc3 != 64) {
                    output = output + String.fromCharCode(chr2);
                }
                if (enc4 != 64) {
                    output = output + String.fromCharCode(chr3);
                }
  
                chr1 = chr2 = chr3 = "";
                enc1 = enc2 = enc3 = enc4 = "";
  
            } while (i < input.length);
  
            return output;
        }
    };
  
    /* jshint ignore:end */
});