'use strict';

var app = angular.module('AngularAuthApp', ['ngRoute', 'LocalStorageModule', 'angular-loading-bar']);

 
app.config(function ($routeProvider) {
 
    $routeProvider.when("/home", {
        controller: "homeController",
        templateUrl: "/views/home.html"
    });
 
    $routeProvider.when("/login", {
        controller: "loginController",
        templateUrl: "/views/login.html"
    });
 
    $routeProvider.when("/gradebook", {
        controller: "gradebookController",
        templateUrl: "views/gradebook.html"
    });
 
    $routeProvider.otherwise({ redirectTo: "/home" });
});
 
app.run(['authService', function (authService) {
    authService.fillAuthData();
}]);


app.config(function ($httpProvider) {
    $httpProvider.interceptors.push('authInterceptorService');
});